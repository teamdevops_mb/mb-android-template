def shouldBuild = true
def replace_file_folder="myapp"     //folder name from mindbowser-devops-files repository
slack_channel = 'jenkins'

pipeline
{
    agent{
        docker{
            image 'mindbowser/android-30-sdk:1.0'
            args '-u root:root'
        }
    }
     environment {
        SLACK_URL = 'https://hooks.slack.com/services/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'
        channel    = "${slack_channel}"
    }
    stages{
         stage('check branch') {
            steps {
                echo "${env.BRANCH_NAME}"
               script{
                  if (env.BRANCH_NAME != 'master' && env.BRANCH_NAME != 'staging' && env.BRANCH_NAME != 'development') {
                    echo 'This is not master or staging or development'
                    env.shouldBuild = false
               }
               }//script
             }//step end
       }//check branch

        stage('Init')
        {
             when{
                    expression {
                        return env.shouldBuild != "false"
                    }
            }//when

             steps {
                script {
                    lastCommitInfo = sh(script: "git log -1", returnStdout: true).trim()
                    commitContainsSkip = sh(script: "git log -1 | grep 'skip ci'", returnStatus: true)
                    slackMessage = "*${env.JOB_NAME}* *${env.BRANCH_NAME}* received a new commit. \nHere is commmit info: ${lastCommitInfo}"

                    //send slack notification of new commit
                    slack_send(slackMessage)

                    //if commit message contains skip ci
                    if(commitContainsSkip == 0) {
                        skippingText = " Skipping Build for *${env.BRANCH_NAME}* branch.."
                        env.shouldBuild = false
                        currentBuild.result = "NOT_BUILT"
                        slack_send(skippingText,"warning")
                    }
                }
            }//step end
        }

         //download neccessary files like dependency and properties file
        stage('download necessary files'){
             when{
                    expression {
                        return env.shouldBuild != "false"
                    }
            }//when

            steps{
                //send slack notification of new commit
                slack_send("fetching properties and configuration files for *${env.BRANCH_NAME}* build.")

              dir("files")
               {
                checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'SSH_BITBUCKET', url: 'git@bitbucket.org:Mindbowser/mindbowser-devops-files.git']]]
               }
            }
        }//download neccssary files end

        stage('Build')
        {
            when{
                expression {
                    return env.shouldBuild != "false"
                }
            }
            stages{
                stage('preparation')
                {
                    steps{
                        script
                            {
                                branch_name = "${env.BRANCH_NAME}"

                                if(branch_name == "master")
                                {
                                    //temp added staging credentials. Change when release . [Need Update]
                                    env.BUILD_TYPE = 'release'
                                    withCredentials([string(credentialsId: 'RELEASE_APP_ID', variable: 'app_id')]) {
                                        env.APP_ID=app_id
                                    }
                                    //add relase firebase token  [Need Update]
                                    withCredentials([string(credentialsId: 'FIREBASE_TOKEN', variable: 'token')]) {
                                        env.FIREBASE_TOKEN = token
                                    }
                                }else if(branch_name == "development")
                                {
                                    env.BUILD_TYPE = 'debug'
                                    withCredentials([string(credentialsId: 'DEV_APP_ID', variable: 'app_id')]) {
                                        env.APP_ID=app_id
                                    }
                                    withCredentials([string(credentialsId: 'FIREBASE_TOKEN', variable: 'token')]) {
                                        env.FIREBASE_TOKEN = token
                                    }
                                }else if(branch_name == "staging")
                                {
                                    env.BUILD_TYPE = 'staging'
                                    withCredentials([string(credentialsId: 'STAGING_APP_ID', variable: 'app_id')]) {
                                        env.APP_ID=app_id
                                    }
                                    withCredentials([string(credentialsId: 'FIREBASE_TOKEN', variable: 'token')]) {
                                      env.FIREBASE_TOKEN = token
                                  }//credentials

                                }

                            }//script
                    }//steps
                }//preparation
                stage('Build Application')
                {
                    steps{

                        //change permission of gradlew and Gemfile
                        sh "chmod +x gradlew"
                        sh "chmod +x Gemfile"

                        sh "mkdir -p app/src/${env.BUILD_TYPE}/"
                        //replace files in the buildtype folder like google-services.json
                        sh  "cp -rf --no-preserve=mode,ownership files/${replace_file_folder}/${env.BUILD_TYPE}/* app/src/${env.BUILD_TYPE}/"


                        sh "mkdir -p config/"
                        //replace config files like staging.properties
                        sh  "cp -rf --no-preserve=mode,ownership files/${replace_file_folder}/config/* config/"

                        //copy keystore file to app and root directory
                        sh  "cp -rf --no-preserve=mode,ownership files/${replace_file_folder}/keystore/* app/"
                        sh  "cp -rf --no-preserve=mode,ownership files/${replace_file_folder}/keystore/* ."

                        slack_send("*${env.BUILD_TYPE}* properties and configuration files are replaced successfully.")

                        slack_send("Building *${env.BUILD_TYPE}* variant.")


                        //build and upload to firebase
                        sh "fastlane ${env.BUILD_TYPE} firebase_token:${env.FIREBASE_TOKEN}"

                        slack_send("Build for *${env.BRANCH_NAME}* completed successfully. ${env.BUILD_TYPE} variant uploaded successfully to firebase distribution.","#0066ff")

                    }//steps end
                }//build application step

            }//nested stages

        }//build stage
        stage('Clean')
        {
            when{
                expression {
                    return env.shouldBuild != "false"
                }
            }
            steps{

                sh 'fastlane clean'
            }
        }//clean build

    }//stages

    post {
        always {
            sh "chmod -R 777 ."
            deleteDir()
        }
        failure {
            slack_send("*${env.BRANCH_NAME}* Build Failed.","danger")
        }
    }

}//pipeline


//change channel name
def slack_send(slackMessage,messageColor="good")
{
    slackSend channel: slack_channel , color: messageColor, message: slackMessage
}